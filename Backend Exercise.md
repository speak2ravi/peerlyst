### Problem Statement
We want to show each Peerlyst.com user a feed of posts that could be relevant to their interests.


### Given
1. Authenticated users can post content (User Post).
2. There are also special posts by Peerlyst's community managers (Peerlyst Post). Let's say there are 2 types of Peerlyst Posts – Type A and B.
3. Each post can have 0 or more tags.
4. While publishing a Post, the user can choose from a global pool of tags or add new ones.
5. A User can "follow" other users, as well tags.

### Requirement Specification
1. My feed should show posts by users I follow, and also posts associated with tags I follow.
2. The feed should be interspersed with Peerlyst Posts. The sequence of posts should satisfy the following rules:
 * First 2 posts of the feed should be User Posts based on my follows
 * The next 2 posts should be Peerlyst Posts of type A and type B, in that order.
 * The cycle should repeat as long as there are posts to be shown in the feed.
 * If any type of post is exhausted, the next in sequence should be shown. e.g. if I have only 3 posts based on follows, the sequence should be like:
    - `User Post by Follow`
    - `User Post by Follow`
    - `Peerlyst Post Type A`
    - `Peerlyst Post Type B`
    - `User Post by Follow`
    - `Peerlyst Post Type A`
    - `Peerlyst Post Type B`
    - `Peerlyst Post Type A`
    - `...`
 * Posts should be sorted by recency, and displayed in batches of 10 (via pagination or infinite scrolling)
3. Out of scope:
 * UI. Something very basic for demonstration would be appreciated though.
 * Editing and Deletion of posts
 * "Unfollowing" users and tags

### Exercise
Write a NodeJs implementation for the above requirements, assuming a MongoDB storage, preferably using Meteorjs. Deliverables should include:
1. Domain model and Data Access layer, including Schema if applicable.
2. Meteor Method or a Web Service for handling the event when a new Post is published.
3. Meteor publication or web service for fetching the feed data for a given user.
4. Do your best to make it self-explanatory. Any special techniques used for this purpose will fetch extra points.
5. BDD/TDD would fetch extra points, but only if done properly :)
6. Note the time required to implement each aspect, and submit with the solution.
7. Feel free to ask questions if needed.

Good luck!
