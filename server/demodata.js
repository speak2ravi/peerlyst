/**
 * Created by betha on 1/7/2017.
 */

Meteor.startup(function(){

  if(Tags.find().count()===0){
    var data_tags = [
      {_id:"peerlysttagid000000001",tag:"Security Risks"},
      {_id:"peerlysttagid000000002",tag:"Messaging Threats"},
      {_id:"peerlysttagid000000003",tag:"Mobile Security"},
      {_id:"peerlysttagid000000004",tag:"Security Resources"},
      {_id:"peerlysttagid000000005",tag:"Firewall Security"},
      {_id:"peerlysttagid000000006",tag:"Network Intrusion"},
      {_id:"peerlysttagid000000007",tag:"SSL"},
      {_id:"peerlysttagid000000008",tag:"PKI"},
      {_id:"peerlysttagid000000009",tag:"Biometric"},
      {_id:"peerlysttagid000000010",tag:"VPN Security"},
      {_id:"peerlysttagid000000011",tag:"Routers"},

    ]
    _.each(data_tags, function(tag) {
      Tags.insert(tag);
    });
  }

  if(Meteor.users.find().count()===0){
    var users = [
      { "_id" : "JG4umuWXayroEGyBh","services" : { "password" : { "bcrypt" : "$2a$10$9CzGzCw/avA.JgF5tB8gRug3xUrArd94ibfoJ9CR/raKewCulhjre" }, "resume" : { "loginTokens" : [ {
        "hashedToken" : "07eM1II8UD0DPOdK3u6WmydLu0AmnC3a+fCkWaVJqLY=" } ] } }, "emails" : [ { "address" : "betha.ravikiran@gmail.com", "verified" : false } ] },
      { "_id" : "HG4umuWXayroEGyBh","services" : { "password" : { "bcrypt" : "$2a$10$9CzGzCw/avA.JgF5tB8gRug3xUrArd94ibfoJ9CR/raKewCulhjre" }, "resume" : { "loginTokens" : [ {
        "hashedToken" : "07eM1II8UD0DPOdK3u6WmydLu0AmnC3a+fCkWaVJqLY=" } ] } }, "emails" : [ { "address" : "betha.ravikiran@gmail.com", "verified" : false } ] },

      { "_id" : "KG4umuWXayroEGyBh","services" : { "password" : { "bcrypt" : "$2a$10$9CzGzCw/avA.JgF5tB8gRug3xUrArd94ibfoJ9CR/raKewCulhjre" }, "resume" : { "loginTokens" : [ {
        "hashedToken" : "07eM1II8UD0DPOdK3u6WmydLu0AmnC3a+fCkWaVJqLY=" } ] } }, "emails" : [ { "address" : "betha.ravikiran@gmail.com", "verified" : false } ] },

      { "_id" : "LG4umuWXayroEGyBh","services" : { "password" : { "bcrypt" : "$2a$10$9CzGzCw/avA.JgF5tB8gRug3xUrArd94ibfoJ9CR/raKewCulhjre" }, "resume" : { "loginTokens" : [ {
        "hashedToken" : "07eM1II8UD0DPOdK3u6WmydLu0AmnC3a+fCkWaVJqLY=" } ] } }, "emails" : [ { "address" : "betha.ravikiran@gmail.com", "verified" : false } ] },

      { "_id" : "MG4umuWXayroEGyBh", "services" : { "password" : { "bcrypt" : "$2a$10$9CzGzCw/avA.JgF5tB8gRug3xUrArd94ibfoJ9CR/raKewCulhjre" }, "resume" : { "loginTokens" : [ {
        "hashedToken" : "07eM1II8UD0DPOdK3u6WmydLu0AmnC3a+fCkWaVJqLY=" } ] } }, "emails" : [ { "address" : "betha.ravikiran@gmail.com", "verified" : false } ] },
    ]
  }

  if(Feed.find().count()===0){
    var data_posts = [
      {owner_id:"AZ4umuWXayroEGyBh",post_type:"peerlystA",post_text:"This Post Type A 1",tag_ids:["peerlysttagid000000001","peerlysttagid000000002"]},
      {owner_id:"AZ4umuWXayroEGyBh",post_type:"peerlystA",post_text:"This Post Type A 2",tag_ids:["peerlysttagid000000001","peerlysttagid000000002"]},
      {owner_id:"AZ4umuWXayroEGyBh",post_type:"peerlystA",post_text:"This Post Type A 3",tag_ids:["peerlysttagid000000003","peerlysttagid000000005"]},
      {owner_id:"AZ4umuWXayroEGyBh",post_type:"peerlystA",post_text:"This Post Type A 4",tag_ids:["peerlysttagid000000001","peerlysttagid000000002"]},
      {owner_id:"AZ4umuWXayroEGyBh",post_type:"peerlystA",post_text:"This Post Type A 5",tag_ids:["peerlysttagid000000001","peerlysttagid000000002"]},
      {owner_id:"AZ4umuWXayroEGyBh",post_type:"peerlystA",post_text:"This Post Type A 6",tag_ids:["peerlysttagid000000001","peerlysttagid000000002"]},
      {owner_id:"AZ4umuWXayroEGyBh",post_type:"peerlystA",post_text:"This Post Type A 7",tag_ids:["peerlysttagid000000011","peerlysttagid000000002"]},
      {owner_id:"AZ4umuWXayroEGyBh",post_type:"peerlystA",post_text:"This Post Type A 8",tag_ids:["peerlysttagid000000009","peerlysttagid000000002"]},
      {owner_id:"AZ4umuWXayroEGyBh",post_type:"peerlystA",post_text:"This Post Type A 9",tag_ids:["peerlysttagid000000008","peerlysttagid000000002"]},
      {owner_id:"AZ4umuWXayroEGyBh",post_type:"peerlystA",post_text:"This Post Type A 10",tag_ids:["peerlysttagid000000007","peerlysttagid000000002"]},
      {owner_id:"AZ4umuWXayroEGyBh",post_type:"peerlystA",post_text:"This Post Type A 11",tag_ids:["peerlysttagid000000006","peerlysttagid000000002"]},
      {owner_id:"AZ4umuWXayroEGyBh",post_type:"peerlystA",post_text:"This Post Type A 12",tag_ids:["peerlysttagid000000005","peerlysttagid000000002"]},
      {owner_id:"AZ4umuWXayroEGyBh",post_type:"peerlystA",post_text:"This Post Type A 13",tag_ids:["peerlysttagid000000004","peerlysttagid000000002"]},
      {owner_id:"AZ4umuWXayroEGyBh",post_type:"peerlystA",post_text:"This Post Type A 14",tag_ids:["peerlysttagid000000003","peerlysttagid000000002"]},

      {owner_id:"JG4umuWXayroEGyBh",post_type:"user",post_text:"This is a post 1 by user ",tag_ids:["peerlysttagid000000001","peerlysttagid000000002"]},
      {owner_id:"JG4umuWXayroEGyBh",post_type:"user",post_text:"This is a post 2 by user ",tag_ids:["peerlysttagid000000001","peerlysttagid000000002"]},
      {owner_id:"HG4umuWXayroEGyBh",post_type:"user",post_text:"This is a post 3 by user ",tag_ids:["peerlysttagid000000003","peerlysttagid000000005"]},
      {owner_id:"HG4umuWXayroEGyBh",post_type:"user",post_text:"This is a post 4 by user ",tag_ids:["peerlysttagid000000001","peerlysttagid000000002"]},
      {owner_id:"JG4umuWXayroEGyBh",post_type:"user",post_text:"This is a post 5 by user ",tag_ids:["peerlysttagid000000001","peerlysttagid000000002"]},
      {owner_id:"JG4umuWXayroEGyBh",post_type:"user",post_text:"This is a post 6 by user ",tag_ids:["peerlysttagid000000001","peerlysttagid000000002"]},
      {owner_id:"JG4umuWXayroEGyBh",post_type:"user",post_text:"This is a post 7 by user ",tag_ids:["peerlysttagid000000011","peerlysttagid000000002"]},
      {owner_id:"KG4umuWXayroEGyBh",post_type:"user",post_text:"This is a post 8 by user ",tag_ids:["peerlysttagid000000009","peerlysttagid000000002"]},
      {owner_id:"KG4umuWXayroEGyBh",post_type:"user",post_text:"This is a post 9 by user ",tag_ids:["peerlysttagid000000008","peerlysttagid000000002"]},
      {owner_id:"MG4umuWXayroEGyBh",post_type:"user",post_text:"This is a post 10 by user ",tag_ids:["peerlysttagid000000007","peerlysttagid000000002"]},
      {owner_id:"JG4umuWXayroEGyBh",post_type:"user",post_text:"This is a post 11 by user ",tag_ids:["peerlysttagid000000006","peerlysttagid000000002"]},
      {owner_id:"LG4umuWXayroEGyBh",post_type:"user",post_text:"This is a post 12 by user ",tag_ids:["peerlysttagid000000005","peerlysttagid000000002"]},
      {owner_id:"LG4umuWXayroEGyBh",post_type:"user",post_text:"This is a post 13 by user ",tag_ids:["peerlysttagid000000004","peerlysttagid000000002"]},
      {owner_id:"MG4umuWXayroEGyBh",post_type:"user",post_text:"This is a post 14 by user ",tag_ids:["peerlysttagid000000003","peerlysttagid000000002"]},

      {owner_id:"BZ4umuWXayroEGyBh",post_type:"peerlystB",post_text:"This Post Type B 1",tag_ids:["peerlysttagid000000001","peerlysttagid000000002"]},
      {owner_id:"BZ4umuWXayroEGyBh",post_type:"peerlystB",post_text:"This Post Type B 2",tag_ids:["peerlysttagid000000001","peerlysttagid000000002"]},
      {owner_id:"BZ4umuWXayroEGyBh",post_type:"peerlystB",post_text:"This Post Type B 3",tag_ids:["peerlysttagid000000003","peerlysttagid000000005"]},
      {owner_id:"BZ4umuWXayroEGyBh",post_type:"peerlystB",post_text:"This Post Type B 4",tag_ids:["peerlysttagid000000001","peerlysttagid000000002"]},
      {owner_id:"BZ4umuWXayroEGyBh",post_type:"peerlystB",post_text:"This Post Type B 5",tag_ids:["peerlysttagid000000001","peerlysttagid000000002"]},
      {owner_id:"BZ4umuWXayroEGyBh",post_type:"peerlystB",post_text:"This Post Type B 6",tag_ids:["peerlysttagid000000001","peerlysttagid000000002"]},
      {owner_id:"BZ4umuWXayroEGyBh",post_type:"peerlystB",post_text:"This Post Type B 7",tag_ids:["peerlysttagid000000011","peerlysttagid000000002"]},
      {owner_id:"BZ4umuWXayroEGyBh",post_type:"peerlystB",post_text:"This Post Type B 8",tag_ids:["peerlysttagid000000009","peerlysttagid000000002"]},
      {owner_id:"BZ4umuWXayroEGyBh",post_type:"peerlystB",post_text:"This Post Type B 9",tag_ids:["peerlysttagid000000008","peerlysttagid000000002"]},
      {owner_id:"BZ4umuWXayroEGyBh",post_type:"peerlystB",post_text:"This Post Type B 10",tag_ids:["peerlysttagid000000007","peerlysttagid000000002"]},
      {owner_id:"BZ4umuWXayroEGyBh",post_type:"peerlystB",post_text:"This Post Type B 11",tag_ids:["peerlysttagid000000006","peerlysttagid000000002"]},
      {owner_id:"BZ4umuWXayroEGyBh",post_type:"peerlystB",post_text:"This Post Type B 12",tag_ids:["peerlysttagid000000005","peerlysttagid000000002"]},
      {owner_id:"BZ4umuWXayroEGyBh",post_type:"peerlystB",post_text:"This Post Type B 13",tag_ids:["peerlysttagid000000004","peerlysttagid000000002"]},
      {owner_id:"BZ4umuWXayroEGyBh",post_type:"peerlystB",post_text:"This Post Type B 14",tag_ids:["peerlysttagid000000003","peerlysttagid000000002"]}
    ]
    _.each(data_posts, function(post) {
      Feed.insert(post);
      Meteor._sleepForMs(1000);
    });
  }



  if(InterestPoints.find().count()===0){
    var data_interestPoints = [
      {owner_id:"JG4umuWXayroEGyBh",follows_ids:["HG4umuWXayroEGyBh","LG4umuWXayroEGyBh"],tag_ids:["peerlysttagid000000001","peerlysttagid000000002"]},
    ]
    _.each(data_interestPoints, function(interestPoint) {
      InterestPoints.insert(interestPoint);
    });
  }

});
