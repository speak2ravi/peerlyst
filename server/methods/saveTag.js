/**
 * Created by betha on 1/7/2017.
 */
if (Meteor.isServer) {

  Meteor.methods({
    saveTag: function(data) {
      if (data.newTag.hasOwnProperty('addTag')) {

        var tag = data.newTag.addTag;

        var insertTag = Tags.insert({tag:tag});

        if (insertTag) {
          var rtrn = '{"status": "Success","Message":"New Tag was added to the list."}';
          return rtrn;
        }
        else{
          var rtrn = '{"status": "Failed","Message":"Failed to insert new tag to the list."}';
          return rtrn;
        }
      } else {
        var rtrn = '{"status": "Error" , "Message": "Required parameters missing"}';
        return rtrn;
      }
    },
  });
}
