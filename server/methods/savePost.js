/**
 * Created by betha on 1/7/2017.
 */
if (Meteor.isServer) {

  Meteor.methods({
    savePost: function(data) {
      if (data.newPost.hasOwnProperty('owner_id') && data.newPost.hasOwnProperty('post_text')) {

       var newPost= {
         owner_id:data.newPost.owner_id,
         post_type:'user',
         post_text:data.newPost.post_text,
         tag_ids:data.newPost.tag_ids
       }

        var insertPost = Feed.insert(newPost);

        if (insertPost) {
          var rtrn = '{"status": "Success","Message":"Post was inserted successfully"}';
          return rtrn;
        }
        else{
          var rtrn = '{"status": "Failed","Message":"Failed to insert post."}';
          return rtrn;
        }
      } else {
        var rtrn = '{"status": "Error" , "Message": "Required parameters missing"}';
        return rtrn;
      }
    },
  });
}
