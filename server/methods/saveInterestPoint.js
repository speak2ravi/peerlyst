/**
 * Created by betha on 1/7/2017.
 */
if (Meteor.isServer) {

  Meteor.methods({
    saveInterestPoint: function(data) {
      if (data.interestPoint.hasOwnProperty('owner_id')) {

        // Create an event
        var owner_id = data.interestPoint.owner_id;
        var tag_ids = data.interestPoint.tag_ids;
        var follows_ids = data.interestPoint.follows_ids;

        console.log(data.interestPoint);


        var upsertEvent = InterestPoints.update({owner_id:owner_id},
        {
            $set: {owner_id:owner_id,tag_ids:tag_ids,follows_ids:follows_ids}
        },
        { upsert: true }
        );

        if (upsertEvent) {
          var rtrn = '{"status": "Success","Message":"Interest points are saved for the user."}';
          return rtrn;
        }
        else{
          var rtrn = '{"status": "Failed","Message":"Failed to save the user interest points."}';
          return rtrn;
        }
      } else {
        var rtrn = '{"status": "Error" , "Message": "Required parameters missing"}';
        return rtrn;
      }
    },
  });
}
