/**
 * Created by betha on 1/7/2017.
 */
Meteor.publish('postFeed', function(user_id,pageIndex) {
  var self = this;

  // defines the scope of data to be loaded, primarily based on page index provided by the client
  var batchSize =10;
  var bucketSize =  batchSize*pageIndex;

  // Collects the record identifiers in an array
  var postsBucket = [];
  var postsInBucket = 0;

  var userPostSkip = 0;
  var postASkip =0;
  var postBSkip =0;

  // Our data filtering criteria
  var user_interestPoint  = InterestPoints.findOne({owner_id:user_id});

  if(typeof user_interestPoint!=="undefined"){

    if(typeof user_interestPoint.tag_ids!=="undefined"){
        var tag_ids = user_interestPoint.tag_ids;
    }
    else{

      var tag_ids =[];
    }

    if(typeof user_interestPoint.follows_ids!=="undefined"){
        var follows_ids = user_interestPoint.follows_ids;
    }
    else{
      var follows_ids =[];
    }

    var isMoreData =true;

    while(postsInBucket < bucketSize && isMoreData){

      var newUserPostRecs =0;
      var newPostARecs =0;
      var newPostBRecs =0;

      // User Posts for the Tags and Follows ids
      var userPosts = Feed.find({
                                  post_type:'user',
                                  $or:[{owner_id:{$in:follows_ids}},{tag_ids:{$in:tag_ids}}]
                                },
                                {
                                  sort: {created_at:-1},
                                  skip: userPostSkip,
                                  limit: 2
                                }
                                ).fetch();

      newUserPostRecs = userPosts.length;

      userPosts.forEach(function(rec){
        if(postsInBucket < bucketSize){
          postsBucket.push(rec._id);
          userPostSkip++;
          postsInBucket++;
        }
      })

      // Peerlyst A Types
      var peerlystATypes = Feed.find({
                                      post_type:'peerlystA',
                                      $or:[{owner_id:{$in:follows_ids}},{tag_ids:{$in:tag_ids}}]
                                     },
                                    {
                                      sort: {created_at:-1},
                                      skip: postASkip,
                                      limit: 2
                                    }
                                    ).fetch();

      newPostARecs = peerlystATypes.length;

      peerlystATypes.forEach(function(rec){
        if(postsInBucket < bucketSize) {
          postsBucket.push(rec._id);
          postASkip++;
          postsInBucket++;
        }
      })

      // List of Peerlyst B Types
      var peerlystBTypes = Feed.find({
                                      post_type:'peerlystB',
                                      $or:[{owner_id:{$in:follows_ids}},{tag_ids:{$in:tag_ids}}]
                                     },
                                    {
                                      sort: {created_at:-1},
                                      skip: postBSkip,
                                      limit: 2
                                    }
                                  ).fetch();

      newPostBRecs = peerlystBTypes.length;

      peerlystBTypes.forEach(function(rec){
        if(postsInBucket < bucketSize) {
          postsBucket.push(rec._id);
          postBSkip++;
          postsInBucket++;
        }
      })

      // Logic to break the look incase there were not enough records.
      var totalNewRecs = newUserPostRecs+newPostARecs+newPostBRecs;
      if(totalNewRecs===0){
        isMoreData = false;
      }
    }

    // Based on list of record ids collected for all the kinds of posts,
    // pull the qualified posts.
    var feedData = Feed.find({_id:{$in:postsBucket}})

    // Since our order of records read from collection was in descending order of time,
    // we have to reverse our reading ordering in the final results and hence we assign
    // decreasing number for the temp index to be used in the client
    feedData.forEach(function(rec){
      var index= postsBucket.indexOf(rec._id);
      var ctr = postsBucket.length - index;
      _.extend(rec, { temp_idx : ctr });
      self.added("feed", rec._id, rec);
    })

    return feedData;
  }
  else{
    // Returns default list of post in the order of date and time
    return Feed.find({},{sort: {created_at:-1},limit:bucketSize});
  }

});

