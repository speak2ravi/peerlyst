/**
 * Created by betha on 1/7/2017.
 */
InterestPointSub = new SubsManager({cacheLimit:1});

var selectedTags = new ReactiveArray();;
var selectedUsers = new ReactiveArray();

Template.interestPoint.onCreated(function(){

  var instance = this;

  instance.interestPointReady = new ReactiveVar();

  this.autorun(function () {

    var handleInterestPoints = InterestPointSub.subscribe('interestPoint',Meteor.userId());
    instance.interestPointReady.set(handleInterestPoints.ready());

  });
});


Template.interestPoint.helpers({
  Tags:function(){
    return Tags.find({});
  },

  Users:function(){
    var users =[];

    var  userList = Meteor.users.find({});

    userList.forEach(function(user){
      users.push({user_id:user._id,username:user.username});
    })

    return users;
  },

  SelectedTags:function(){

    if(selectedTags.length===0) {
      var interestPoint = InterestPoints.findOne();

      if (typeof interestPoint !== "undefined") {
        if (typeof interestPoint.tag_ids !== "undefined") {
          var tags = Tags.find({_id: {$in: interestPoint.tag_ids}});
          tags.forEach(function (tag) {
            selectedTags.push(tag);
          })
        }
      }
    }

    return selectedTags.list();
  },

  SelectedUsers:function(){

    if(selectedUsers.length===0){
      var interestPoint = InterestPoints.findOne();

      if(typeof interestPoint!=="undefined"){
        if(typeof interestPoint.follows_ids !=="undefined"){

          var users =  Meteor.users.find({_id:{$in:interestPoint.follows_ids}});
          users.forEach(function(user){
            selectedUsers.push(user);
          })
        }
      }
    }


    return selectedUsers.list();
  }
});


Template.interestPoint.events({
  'change #tag_select':function(event){

    var tag_id = $('#tag_select').val();
    var tag =  Tags.findOne({_id:tag_id});
    if(selectedTags.indexOf(tag)==-1){
      selectedTags.push(tag);
    }
  },

  'change #user_select':function(event){

    var username = $('#user_select').val();
    var user =  Meteor.users.findOne({username:username});
    if(selectedUsers.indexOf(user)==-1){
      selectedUsers.push(user);
    }
  },

  'click #btn_add_tag':function(event){
    event.preventDefault();
    Router.go("addTag");
  },

  'click #save_interests':function(event){
    event.preventDefault();

    var tags =[];

    selectedTags.forEach(function(tag){
      tags.push(tag._id);
    });

    var users =[];

    selectedUsers.forEach(function(user){
      users.push(user._id);
    });

    var owner_id = Meteor.userId();

    var interestPoint = {
      owner_id: owner_id,
      tag_ids: tags,
      follows_ids: users,
    };

    Meteor.call('saveInterestPoint', {interestPoint: interestPoint}, function (err, response) {
      if (err) {
        alert("Error" + err.message);
      }
      else{
        Session.setPersistent("pageIndex",1);
        Router.go('postList');
      }
    });
  }
});
