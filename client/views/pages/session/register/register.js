/**
 * Created by betha on 1/7/2017.
 */
Template.register.events({
  'submit form': function(event) {
    event.preventDefault();
    var userNameVar = event.target.userName.value;
    var emailVar = event.target.registerEmail.value;
    var passwordVar = event.target.registerPassword.value;
    Accounts.createUser({
      username:userNameVar,
      email: emailVar,
      password: passwordVar
    });
  }
});
