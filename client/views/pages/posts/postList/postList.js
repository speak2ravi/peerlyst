/**
 * Created by betha on 1/7/2017.
 */
Template.postList.onCreated(function(){
  var pageIndex = Session.setPersistent("pageIndex",1);
});


Template.postList.events({
  'click #btn_add':function(){
     Router.go('addPost');
  },
  'click #logout':function(){
    Meteor.logout();
    Router.go('landing');
  },
  'click #load_more':function(){
    var pageIndex = Session.get("pageIndex");
    Session.setPersistent("pageIndex",pageIndex+1);
  }
});

Template.postList.helpers({
  userName:function(){
    return Meteor.username;
  }
})

