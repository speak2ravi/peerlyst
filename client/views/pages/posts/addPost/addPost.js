/**
 * Created by betha on 1/7/2017.
 */

TagsSub = new SubsManager({cacheLimit:1});

var selectedTags = new ReactiveArray();

Template.addPost.onCreated(function(){
  var instance = this;

  instance.tagsReady = new ReactiveVar();



  this.autorun(function () {
    var handleTags = TagsSub.subscribe('tags');
    instance.tagsReady.set(handleTags.ready());
  });
});


Template.addPost.helpers({
  Tags:function(){
    return Tags.find({});
  },

  SelectedTags:function(){
    return selectedTags.list();
  }
});


Template.addPost.events({
  'submit form': function(event){
    event.preventDefault();

    var owner_id = Meteor.userId();

    var postText = event.target.postText.value;

    var tag_ids = [];

    _.each(selectedTags, function(tag) {
      tag_ids.push(tag._id);
    });

    var newPost ={
      owner_id:owner_id,
      post_text:postText,
      tag_ids:tag_ids
    }

    Meteor.call('savePost', {newPost: newPost}, function (err, response) {
      if (err) {
        alert("Error" + err.message);
      }
      else{
        Session.setPersistent("pageIndex",1);
        Router.go('postList');
      }
    });
  },

  'change #tag_select':function(event){

     var tag_id = $('#tag_select').val();
     var tag =  Tags.findOne({_id:tag_id});
     if(selectedTags.indexOf(tag)==-1){
       selectedTags.push(tag);
     }
   },

  'click #btn_cancel':function(event){
    event.preventDefault();
    Router.go('postList');
  }
});
