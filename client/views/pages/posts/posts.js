/**
 * Created by betha on 1/7/2017.
 */
PostsSub = new SubsManager({cacheLimit:1}); // We maintaning single sub as we re get the whole data with increased page index as there is a possibility of new posts being added in the mean time.

Template.posts.onCreated(function(){
  var instance = this;

  instance.postsReady = new ReactiveVar();
  instance.rows = new ReactiveVar(10);

  this.autorun(function () {
    var pageIndex = Session.get("pageIndex");
    var handlePosts = PostsSub.subscribe('postFeed',Meteor.userId(),pageIndex);
    instance.postsReady.set(handlePosts.ready());
  });
});


Template.posts.helpers({

  Posts:function(){
    return Feed.find({},{sort:{temp_idx:-1}});
  },
});


Template.post.helpers({

  Tags:function() {
    return Tags.find({_id:{$in:this.tag_ids}});
  }

})


