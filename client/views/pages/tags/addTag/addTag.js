/**
 * Created by betha on 1/7/2017.
 */
Template.addTag.events({

  'submit form': function(event){
    event.preventDefault();

    var addTag = event.target.addTag.value;

    var newTag ={
      addTag:addTag,
    }

    Meteor.call('saveTag', {newTag: newTag}, function (err, response) {
      if (err) {
        alert("Error" + err.message);
      }
      else{
        Router.go('postList');
      }
    });

  },

  'click #btn_cancel':function(event){
    event.preventDefault();
    Router.go('postList');
  }

});
