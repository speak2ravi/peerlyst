/**
 * Created by betha on 1/7/2017.
 */

TagsSub = new SubsManager({cacheLimit:1});
UsersSub = new SubsManager({cacheLimit:1});

Template.defaultLayout.onCreated(function(){
  var instance = this;
  instance.tagsReady = new ReactiveVar();
  instance.usersReady = new ReactiveVar();

  this.autorun(function () {
    var handleTags = TagsSub.subscribe('tags');
    instance.tagsReady.set(handleTags.ready());

    var handleUsers = UsersSub.subscribe('users');
    instance.usersReady.set(handleUsers.ready());
  });
});


