/**
 * Created by betha on 1/7/2017.
 */
if (Meteor.isClient) {

  Router.configure({
    layoutTemplate: "defaultLayout",
  });


  Router.map(
    function () {
      this.route('landing', {
        path: '/',
        layoutTemplate: 'defaultLayout',
        fastRender: true,
        action: function () {
          this.render();
        }
      });

      this.route("addPost",{
        path: "/addPost",
        layoutTemplate: 'defaultLayout',
        action : function(){
          this.render();
        } });

      this.route("interestPoint",{
        path: "/interestPoint",
        layoutTemplate: 'defaultLayout',
        action : function(){
          this.render();
        } });

      this.route('postList', {
        path: '/postList',
        layoutTemplate: 'defaultLayout',
        fastRender: true,
        action: function () {
          this.render();
        }
      });

      this.route('login', {
        path: '/login',
        layoutTemplate: 'defaultLayout',
        fastRender: true,
        action: function () {
          this.render();
        }
      });

      this.route('register', {
        path: '/register',
        layoutTemplate: 'defaultLayout',
        fastRender: true,
        action: function () {
          this.render();
        }
      });

      this.route('addTag', {
        path: '/addTag',
        layoutTemplate: 'defaultLayout',
        fastRender: true,
        action: function () {
          this.render();
        }
      });
    }
  );
}
