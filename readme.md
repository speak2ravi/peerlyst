Schema is defined in models.js under both folder.
Methods are listed in the server under methods folder.
Publishers are in the server under Publishers folder.

Some thoughts before taking the approach:
========================================
If the client is provided with the logic to organise the data in the required order, the server related implementation will be more efficient if i split the types of posts to different collections
and ask for additional subscription data on need bases. In the approach, the server just publishes additional records from each of the collections, and client takes care of organizing the data in certain order of display.

But, if this was to be from the server side, 
a) Expose a server method that would pick the data from all this collections and return the result set in a single array. The server method implements the logic of pulling the information from these different collections
organizing them in a certain order and returning the data to the client.

b) Use Publisher
   With Publisher, if I have three different collections, then the overhead of assembling the data in the required pattern needs to happen on the client side. So, if we take publisher approach, the results should come
   from a single collection to make it straight forward for the client.
   
Approach taken:   
===============
User publisher to organise the data in the required order and send the data to the client collection based on page index requested in the batches of 10. 

If this is to be done, the with out altering the data on the server, there is a need to tell the client about the reading order. To achieve this, I added an additional optional field for the post to assign a temp index
 field which I assign only in the publisher at the time of publishing. In this field, i assign the order in which the data should be read in the client side.
 
 So, in the client side, the reader just needs to read the data sort order of the assigned field.
 
The server database does not know about this attribute and value and hence does not impact other client's retrieval.


Other Notes:
===========
Assumptions that there are limited no of users to follow
tags are also not very large and hence can be caches on the client collection for quick reference.


Running App:
===========
I added Demo data file that will insert some dummy data to initialize.
At the start, the application will prompt for the user to register and login
with no interest points are setup by the user, the application dumps the most 10 recent records.(Logic works after user configures his interest points)
after user sets this interest points, based on tags and users followed, the data is returned in the required pattern.

Total Effort:
============
9-10 hours
Overall framework +  GUI - 6 hours
Main requirement for pattern -  3-4 hours
(1 hour + exploring options on how to some how assign a way to tell client about the sort order at the time of publishing)     


