/**
 * Created by betha on 1/7/2017.
 */
Feed = new Mongo.Collection('feed'); // All types of posts are in the same Collection, identified differently by post type

Tags = new Mongo.Collection('tags');

InterestPoints = new Mongo.Collection('interestPoints');

Schema = {};
SimpleSchema.debug = true;

Schema.Post = new SimpleSchema({
  owner_id:{
    type: String,
  },
  post_type:{
    type: String,
    allowedValues:['user','peerlystA','peerlystB']
  },
  post_text:{
    type: String,
  },
  tag_ids:{
    type:[String],
    optional:true
  },
  temp_idx:{
    type:Number,
    optional:true
  },
  created_at: {
    type: Date,
    autoValue: function() {
      if (this.isInsert) {
        return new Date;
      } else if (this.isUpsert) {
        return {$setOnInsert: new Date};
      } else {
        this.unset();
      }
    }
  },
});

Feed.attachSchema(Schema.Post, {transform: true});


Schema.Tag = new SimpleSchema({
  tag:{
    type:String,
    label:'Tag',
  }
})

Tags.attachSchema(Schema.Tag,{transform: true});

Schema.InterestPoint = new SimpleSchema({
  owner_id:{
    type: String,
  },
  tag_ids:{
    type:[String]
  },
  follows_ids:{
    type:[String]
  }
});

InterestPoints.attachSchema(Schema.InterestPoint, {transform: true});
